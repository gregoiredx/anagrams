import sys


def find_anagrams_in_file(words_file_path, min_number=1):
    with open(words_file_path, "r") as words_file:
        words = [line.rstrip() for line in words_file]
        return find_anagrams_in_list(words, min_number=min_number)


def find_anagrams_in_list(words, min_number=1):
    anagrams_dict = {}
    for word in words:
        _add_word_to_dict(anagrams_dict, word)
    return sorted([sorted(anagrams) for anagrams in anagrams_dict.values() if len(anagrams) >= min_number])


def _add_word_to_dict(anagrams_dict, word):
    word_letters = _word_to_letters_dict(word)
    word_letters_hashable = tuple(sorted(word_letters.items()))
    if word_letters_hashable in anagrams_dict:
        anagrams_dict[word_letters_hashable].append(word)
    else:
        anagrams_dict[word_letters_hashable] = [word]


def _word_to_letters_dict(word):
    word_letters = {}
    for letter in word:
        if letter not in word_letters:
            word_letters[letter] = 1
        else:
            word_letters[letter] += 1
    return word_letters


if __name__ == '__main__':
    words_file_arg = sys.argv[1] if len(sys.argv) > 1 else "/usr/share/dict/words"
    min_number = int(sys.argv[2]) if len(sys.argv) > 2 else 5
    for anagrams in find_anagrams_in_file(words_file_arg, min_number):
        print(*anagrams)
