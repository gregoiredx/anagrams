import os
import tempfile
import unittest

from anagrams import find_anagrams_in_list, find_anagrams_in_file


class AnagramsTest(unittest.TestCase):

    def test_find_anagrams_in_list(self):
        self.assertEqual(find_anagrams_in_list(["ab", "ba", "z"]), [["ab", "ba"], ["z"]])

    def test_find_anagrams_in_list_with_min(self):
        self.assertEqual(find_anagrams_in_list(["ab", "ba", "z"], 2), [["ab", "ba"]])

    def test_find_anagrams_in_list_sorted(self):
        self.assertEqual(find_anagrams_in_list(["ba", "ab"]), [["ab", "ba"]])
        self.assertEqual(find_anagrams_in_list(["z", "ab"]), [["ab"], ["z"]])

    def test_find_anagrams_in_file(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            words_file_path = os.path.join(tmpdirname, "words")
            with open(words_file_path, "w") as words_file:
                words_file.write("ab\nba\nz")

            self.assertEqual(find_anagrams_in_file(words_file_path), [["ab", "ba"], ["z"]])
            self.assertEqual(find_anagrams_in_file(words_file_path, 2), [["ab", "ba"]])
